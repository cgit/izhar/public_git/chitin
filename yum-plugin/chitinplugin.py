#!/usr/bin/python

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Library General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
# Copyright Red Hat Inc. 2007, 2008
#
# Author: Izhar Firdaus <izhar@fedoraproject.com>
# Examples:
#
# yum chitin-install oneclickinstall-XML-metadata-file
# yum chitin-query details oneclickinstall-XML-metadata-file
# yum chitin-query repositories oneclickinstall-XML-metadata-file
# yum chitin-query packages oneclickinstall-XML-metadata-file
#

import re
from chitin.parser import Metapackage
from chitin.utils import get_child_value,get_element_tag
from yum.plugins import PluginYumExit,TYPE_CORE,TYPE_INTERACTIVE
from yum import logginglevels
import yum.i18n, tempfile
_ = yum.i18n._

requires_api_version = '2.5'
plugin_type = (TYPE_INTERACTIVE,)

## Use this for now
distro='openSUSE'
release='Factory'


def add_tmp_repo(base,name,repourls):
    """
    Function to add temporary repository

    @param  base      The YumBase object for the operation
    @param  name      The repository name
    @param  repourls  A list of urls for the repository

    @return           None
    """
    tfo = tempfile.NamedTemporaryFile()
    repoid = name
    repoconfig = """
[%s]
name=%s
failovermethod=priority
baseurl=%s
enabled=1
""" % (name,name,'\n'.join(repourls))
    tfo.file.write(repoconfig)
    tfo.file.close()
    base.getReposFromConfigFile(tfo.name)

class Query:
    def __init__(self,distro,release):
        self.distro = distro
        self.release = release

    def getNames(self):
        return ['chitin-query']

    def getUsage(self):
        return "[details|repositories|packages] [ file]"

    def getSummary(self):
        return "Query data from OneClickInstall metadata"

    def doCheck(self, base, basecmd, extcmds):
        pass

    def getRepos(self): # so we can act as a "conduit"
        return self.repos

    def show_pkg(self, msg, pkg, md, disp=None):
        print msg, pkg, md

    def show_pkg_exit(self):
        pass

    def doCommand(self, base, basecmd, extcmds):
        action = extcmds[0]
        param = extcmds[1:]
        metapackages = []
        for mp in param:
            metapackages.append(Metapackage(mp,'%s %s' % (self.distro,self.release)))

        if action == 'details':
           for chitin in metapackages:
               print '======================='
               print 'Metadata'
               print '======================='
               for c in chitin.getGroup().getchildren():
                   tagname = get_element_tag(c)
                   if tagname in ['name','summary','description']:
                      print tagname,':',c.text
        if action == 'repositories':
           for chitin in metapackages:
               print '========================'
               print 'Repositories'
               print '========================'
               for r in chitin.getRepositories():
                   print get_child_value(r,'name')
                   print chitin.getRepoURL(get_child_value(r,'name'))
        if action == 'packages':
           for chitin in metapackages:
               print '========================'
               print 'Packages'
               print '========================'
               for p in chitin.getProducts():
                   print p.getchildren()[0].text
        print ''
        return 0, ['']

class Install:
    def __init__(self,distro,release):
        self.distro = distro
        self.release = release

    def getNames(self):
        return ['chitin-install']

    def getUsage(self):
        return "[ file]"

    def getSummary(self):
        return "Install packages from OneClickInstall metadata"

    def doCheck(self, base, basecmd, extcmds):
        pass

    def getRepos(self): # so we can act as a "conduit"
        return self.repos

    def show_pkg(self, msg, pkg, md, disp=None):
        print msg, pkg, md

    def show_pkg_exit(self):
        pass

    def doCommand(self, base, basecmd, extcmds):
        packages = []
        repositories = []
        for mp in extcmds:
            chitin = Metapackage(mp,'%s %s' % (self.distro,self.release))
            for p in chitin.getProducts():
                packages.append(get_child_value(p,'name'))
            for r in chitin.getRepositories():
                repositories.append((get_child_value(r,'name'),chitin.getRepoURL(get_child_value(r,'name'))))
        print packages
        print repositories

        base.verbose_logger.log(logginglevels.INFO_2,
                _("Setting up Install Process"))
        try:
            return base.installPkgs(packages)
        except yum.Errors.YumBaseError, e:
            return 1, [str(e)]

def config_hook(conduit):
    conduit.registerCommand(Query(distro,release))
    conduit.registerCommand(Install(distro,release))

