#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Library General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

import re

def attrsfilter(elements,keywords,operation='or'):
   """
   Function to filter elements by its attributes

   @param   elements      A list of elements to be filtered	
   @param   keywords      A dictionary containing required tags and its values
   @param   operation     A string, either 'or' or 'and', for operation selection

   @return              A list of filtered elements by attributes
   """
   retval = []
   if not keywords: return elements
   if operation == 'or':
       for element in elements:
           for key in keywords:
               if element.attrib.has_key(key):
                  if element.attrib[key] == keywords[key]:
                     retval.append(element)
                     break
   elif operation == 'and':
       for element in elements:
           for key in keywords:
               if not element.attrib.has_key(key): break
               if element.attrib[key] != keywords[key]: break
           retval.append(element)
   return retval

def get_element_tag(element):
    """
    Function to get the element tag without namespace

    @param   element    An element which will be extracted the tag name

    @return             A string, containing the tag name without namespace
    """
    tag = re.match('{.*?}(.*)',element.tag)
    if tag:
       return tag.group(1)
    else:
       return tag

def get_child_value(element,childname):
    """
    Function to get the value of an element's child element

    @param  element    An element to be queried
    @param  childname  A string, containing the tagname of the child

    @return            A string containing child's text value if child is a leaf element, 
                       or a ElementTree element if child is a parent to 1 or more elements
    """
    for c in element.getchildren():
        if get_element_tag(c) == childname:
           if c.getchildren():
             return c
           else:
             return c.text

