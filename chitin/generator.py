# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Library General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
#

# Generator/creator library for One Click Install metadata XML
#
# @author Izhar Firdaus <izhar@fedoraproject.com>

from chitin import ocins

class Product:
    def __init__(self):
        self.name = ""
        self.summary = []
        self.description = []
        self.attrs = {
                     'type':'package',
                     'recommended':True,
                     'architechtures':'all',
                     'action':'install',
                     }

    def add_summary(self,value,lang=None):
        pass

    def add_description(self,value,lang=None):
        pass

    

class Repository:
    def __init__(self,name):
        self.name = name
        self.summary = []
        self.description = []
        self.urls = []
        self.attrs = {
                     'recommended':True,
                     'format':'auto',
                     'producturi':'/',
                     }

    def add_summary(self,value,lang=None):
        pass

    def add_description(self,value,lang=None):
        pass

    def add_url(self,url,score=None,location=None):
        pass

class Group
    def __init__(self,distversion):
        self.distversion = distversion
        self.repositories = []
        self.products = []

    def add_repository(self,repository):
        pass

    def add_product(self,product):
        pass

class MetadataCreator:
    def __init__(self):
        self.groups = []

    def add_group(self,group):
        pass

