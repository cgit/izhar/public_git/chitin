#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Library General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
#

# Parser library for One Click Install metadata XML
#
# @author Izhar Firdaus <izhar@fedoraproject.com>

from xml.etree.ElementTree import ElementTree,Element
import re
from chitin.utils import attrsfilter,get_child_value
from chitin import ocins

class Metapackage:
   """
   Parser class for Chitin Metapackage XML.
   """
   def __init__(self,xmlfile,distversion):
       """
       @param   xmlfile       One Click Install XML file
       @param   distversion   A string containing '$distro $release'

       @return                None
       """
       self.element = ElementTree(Element('metapackage'),open(xmlfile))
       self.distversion = distversion

   def getGroup(self):
       """
       Function to get Group element from Chitin Metadata

       @return    'group' Element of the current distversion
       """
       for group in self.element.getiterator('{%s}%s' % (ocins,'group')):
           if group.attrib['distversion'].lower() == self.distversion.lower():
              return group

   def getRepositories(self,*args,**kwargs):
       """
       Function to get 'repository' elements from the current distversion group

       @kwargs     Keyword arguments for filtering results

       @return     A list containing a filtered list of 'repository' Elements
       """
       repos = self.getGroup().getiterator('{%s}%s' % (ocins,'repository'))
       return attrsfilter(repos,kwargs)

   def getProducts(self,*args,**kwargs):
       """
       Function to get 'product' elements from the current distversion group

       @kwargs     Keyword arguments for filtering results

       @return     A list containing a filtered list of 'product' Elements
       """
       products = self.getGroup().getiterator('{%s}%s' % (ocins,'product'))
       return attrsfilter(products,kwargs)

   def getRepoURL(self,reponame,type=None):
       """
       Function to get 'url' values from current distversion group, and requested reponame

       @param   reponame   The repository name

       @return             A list of URLs from the requested repository, sorted higher score first
       """
       repos = self.getRepositories()
       urls = []
       for r in repos:
           if get_child_value(r,'name').lower() == reponame.lower():
              for url in r.getiterator('{%s}%s' % (ocins,'url')):
                  priority = 0
                  if type:
                     if not url.attrib.has_key('type'): continue
                     if not url.attrib['type'].lower() == type.lower(): continue

                  if url.attrib.has_key('score'): priority = int(url.attrib['score'])
                  urls.append((url.text.strip(),priority))

       def sorturl(x,y):
           if x[1] <= y[1]:
              return +1
           else:
              return -1
       urls.sort(sorturl)
       return [ url[0] for url in urls ]

